<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit();
}

// Load Translations --- Essential Addons for Elementor Pro | Lite
add_action( 'init', 'eael_gphp_load_translations' );
function eael_gphp_load_translations() {
	if ( is_rtl() ) {
		$locale = 'fa_IR';
		$domains = array (
					'essential-addons-elementor', 
					'essential-addons-for-elementor-lite'
					);
		foreach ( $domains as $domain ) {
			$mofile = $domain . '-' . $locale . '.mo';
			$path = plugin_dir_path( __FILE__ ) . 'languages/' . $mofile;
			unload_textdomain( $domain );
			load_textdomain( $domain, $path );
		}
	}
}

// Custom Admin CSS Styles
add_action( 'admin_enqueue_scripts', 'eael_gphp_admin_css' );
function eael_gphp_admin_css() {
	if ( is_rtl() ) {
		wp_enqueue_style( 'eael-gphp-admin-css', plugin_dir_url( __FILE__ ) . 'css/eael-gphp-admin.css' );
	}
}

// Custom Front-End CSS Styles
//add_action( 'wp_enqueue_scripts', 'eael_gphp_front_css' );
function eael_gphp_front_css() {
	if ( is_rtl() ) {
		wp_enqueue_style( 'eael-gphp-front-css', plugin_dir_url( __FILE__ ) . 'css/eael-gphp-front.css' );
	}
}
