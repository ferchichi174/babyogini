<?php

namespace Elementor;

defined('ABSPATH') || exit;

class Ekit_Wb_336 extends Widget_Base {

	public function get_name() {
		return 'ekit_wb_336';
	}


	public function get_title() {
		return esc_html__( 'menu', 'elementskit-lite' );
	}


	public function get_categories() {
		return ['basic'];
	}


	public function get_icon() {
		return 'eicon-cog';
	}


	protected function register_controls() {

		$this->start_controls_section(
			'content_section_336_0',
			array(
				'label' => esc_html__( 'Title', 'elementskit-lite' ),
				'tab'   => Controls_Manager::TAB_CONTENT,
			)
		);

		$this->add_control(
			'ekit_wb_336_text',
			array(
				'label' => esc_html__( 'Text', 'elementskit-lite' ),
				'type'  => Controls_Manager::TEXT,
				'default' =>  esc_html( 'Some Text' ),
				'show_label' => true,
				'label_block' => false,
				'input_type' => 'text',
			)
		);

		$this->end_controls_section();

	}


	protected function render() {
	}


}
